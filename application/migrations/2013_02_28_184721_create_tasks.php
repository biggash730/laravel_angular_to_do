<?php

class Create_Tasks {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		// create the tasks table
		Schema::create('tasks', function($table) {
			$table->engine = 'InnoDB';
		    $table->increments('id');
		    $table->string('title', 128);		    
		    $table->string('description', 128)->nullable();
		    $table->date('start_date');
		    $table->date('end_date')->nullable();		    
		    $table->boolean('is_active')->default(true);
		    $table->integer('user_id')->unsigned();
		    $table->integer('status')->unsigned();
		    $table->integer('created_by')->unsigned();
		    $table->integer('updated_by')->unsigned();
		    $table->timestamps();		    
		});	
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('tasks');
	}

}