<?php
class MyTask extends Eloquent{

	public function create_task($client_data){
		try{

			$inputs = array("title" => $client_data->title, 
				"description" => $client_data->description, 
				"start_date" => $client_data->startDate, 
				"end_date" => $client_data->endDate, 
				"is_active" => $client_data->isActive, 
				"user_id" => $client_data->userId, 
				"status" => $client_data->status, 
				);
			$rules = array("title" => "required|max:128", 
				"description" => "required|max:128", 
				"start_date" => "required", 
				"end_date" => "required", 
				"is_active" => "required|numeric", 
				"user_id" => "required|numeric", 
				"status" => "required|numeric", 
				);

			$validation = MyValidator::validate_user_input($inputs,$rules);
			if($validation->fails())
				return HelperFunction::catch_error(null,false,HelperFunction::format_message($validation->errors->all()));

			$arr = DataHelper::create_audit_entries(HelperFunction::get_user_id());
			$arr["title"] = $client_data->title;
			$arr["description"] = $client_data->description;
			$arr["start_date"] = $client_data->startDate;
			$arr["end_date"] = $client_data->endDate;
			$arr["is_active"] = $client_data->isActive;
			$arr["user_id"] = $client_data->userId;
			$arr["status"] = $client_data->status;

			$inserted_record = DataHelper::insert_record('tasks',$arr,'Task');
			if(!$inserted_record['success'])
				throw new Exception($inserted_record['message']);

			return $inserted_record;
		}catch(Exception $e){
			return HelperFunction::catch_error($e,true,HelperFunction::get_admin_error_msg());
		}
	}

	public function update_task($client_data){
		try{

			$inputs = array("id" => $client_data->id, 
				"title" => $client_data->title, 
				"description" => $client_data->description, 
				"start_date" => $client_data->startDate, 
				"end_date" => $client_data->endDate, 
				"is_active" => $client_data->isActive, 
				"user_id" => $client_data->userId, 
				"status" => $client_data->status, 
				);
			$rules = array("id" => "required|numeric", 
				"title" => "required|max:128", 
				"description" => "required|max:128", 
				"start_date" => "required", 
				"end_date" => "required", 
				"is_active" => "required|numeric", 
				"user_id" => "required|numeric", 
				"status" => "required|numeric", 
				);

			$validation = MyValidator::validate_user_input($inputs,$rules);
			if($validation->fails())
				return HelperFunction::catch_error(null,false,HelperFunction::format_message($validation->errors->all()));

			$arr = DataHelper::update_audit_entries(HelperFunction::get_user_id());
			$arr["id"] = $client_data->id;
			$arr["title"] = $client_data->title;
			$arr["description"] = $client_data->description;
			$arr["start_date"] = $client_data->startDate;
			$arr["end_date"] = $client_data->endDate;
			$arr["is_active"] = $client_data->isActive;
			$arr["user_id"] = $client_data->userId;
			$arr["status"] = $client_data->status;

			$updated_record = DataHelper::update_record('tasks',$arr['id'],$arr,'Task');
			if(!$updated_record['success'])
				throw new Exception($updated_record['message'],1);

			return $updated_record;
		}catch(Exception $e){
			return HelperFunction::catch_error($e,true,HelperFunction::get_admin_error_msg());
		}
	}

	public function delete_task($id){		
		try{

			$deleted_entry = DB::table('tasks')->delete($id);
			return  DataHelper::return_json_data($deleted_entry,true,'Task has been deleted',1);

		}catch(Exception $e){
			return HelperFunction::catch_error($e,true);
		}
	}

	public function get_tasks($client_data){
		try{
			$filter_array = array();
			if(array_key_exists("id", $client_data))
				$filter_array["tasks.id"] = $client_data["id"];
			if(array_key_exists("title", $client_data))
				$filter_array["tasks.title"] = "%".$client_data["title"]."%";
			if(array_key_exists("description", $client_data))
				$filter_array["tasks.description"] = "%".$client_data["description"]."%";
			if(array_key_exists("startDate", $client_data))
				$filter_array["tasks.start_date"] = $client_data["startDate"];
			if(array_key_exists("endDate", $client_data))
				$filter_array["tasks.end_date"] = $client_data["endDate"];
			if(array_key_exists("isActive", $client_data))
				$filter_array["tasks.is_active"] = $client_data["isActive"];
			if(array_key_exists("userId", $client_data))
				$filter_array["tasks.user_id"] = $client_data["userId"];
			if(array_key_exists("status", $client_data))
				$filter_array["tasks.status"] = $client_data["status"];

			$query_result = DB::table('tasks')
			->where(function($query) use ($filter_array){				
				$query = DataHelper::filter_data($query,"tasks.id",$filter_array,"int");
				$query = DataHelper::filter_data($query,"tasks.title",$filter_array,"string","like");
				$query = DataHelper::filter_data($query,"tasks.description",$filter_array,"string","like");
				$query = DataHelper::filter_data($query,"tasks.start_date",$filter_array,"date");
				$query = DataHelper::filter_data($query,"tasks.end_date",$filter_array,"date");
				$query = DataHelper::filter_data($query,"tasks.is_active",$filter_array,"int");
				$query = DataHelper::filter_data($query,"tasks.user_id",$filter_array,"int");
				$query = DataHelper::filter_data($query,"tasks.status",$filter_array,"int");

			});

			$total = $query_result->count();

			$query_result = $query_result->order_by('tasks.id','desc');

			$paging_result = array_key_exists('page', $client_data) ? 
			HelperFunction::paginate($client_data['page'],$client_data['limit'], $query_result):$query_result;

        	//execute query and specify columns to retrive
			$result = $paging_result->get(
				array("tasks.id",
					"tasks.title",
					"tasks.description",
					"tasks.start_date",
					"tasks.end_date",
					"tasks.is_active",
					"tasks.user_id",
					"tasks.status",
					"tasks.created_by",
					"tasks.updated_by",
					"tasks.created_at",
					"tasks.updated_at",
					)
				);

			$out = array_map(function($data){
				
				$arr = array();
				$arr["id"] = $data->id;
				$arr["title"] = $data->title;
				$arr["description"] = $data->description;
				$arr["startDate"] = HelperFunction::format_date_to_client($data->start_date);
				$arr["endDate"] = HelperFunction::format_date_to_client($data->end_date);
				$arr["isActive"] = $data->is_active;
				$arr["userId"] = $data->user_id;
				$arr["status"] = $data->status;
				$arr["createdBy"] = $data->created_by;
				$arr["updatedBy"] = $data->updated_by;
				$arr["createdAt"] = HelperFunction::format_date_to_client($data->created_at);
				$arr["updatedAt"] = HelperFunction::format_date_to_client($data->updated_at);

				return $arr;
			},$result);

			return HelperFunction::return_json_data($out,true,'record loaded',$total);
		}catch(Exception $e){
			return HelperFunction::catch_error($e,true,HelperFunction::get_admin_error_msg());
		}
	}
}
