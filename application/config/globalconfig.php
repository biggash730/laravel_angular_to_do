<?php

return array(

			/*
			|--------------------------------------------------------------------------
			| Flag to determine the type of messages to display.
			|--------------------------------------------------------------------------
			|By default the application assumes  it is in a development 
			|environment  so technical or exception messages are displayed on the client side.The application 
			|will display a user friendly message if environment = 'production' is set.
			|
			*/
			'environment' => 'production',
			/*
			|--------------------------------------------------------------------------
			| Generic friendly message to send to the client side when an error ocurrs
			|--------------------------------------------------------------------------
			|Generic Error message to display to a user when an error occures in a production 
			|environment.Techinical messages will always be displayed when app is in 
			|development mode.
			*/
			'global_error_message' => 'an error occurred.contact your admin',
			/*
			|--------------------------------------------------------------------------
			| Default Ticket_Status Id
			|--------------------------------------------------------------------------
			|Default ticket_status_id as auto-generated in the db table ticket_statuses
			|
			|
			*/
			'default_ticket_status'=>1,
			/*
			|--------------------------------------------------------------------------
			| Closed ticket_status Id
			|--------------------------------------------------------------------------
			|The id of a closed ticket_status in the ticket_statuses table
			|
			|
			*/
			'closed_ticket_status_id'=>2,
			/*
			|--------------------------------------------------------------------------
			| Resolved ticket_status Id
			|--------------------------------------------------------------------------
			|The id of a resolved ticket in the ticket_statuses table
			|
			|
			*/
			'resolved_ticket_status_id'=>3,
			/*
			|--------------------------------------------------------------------------
			| UnResolved ticket_status Id
			|--------------------------------------------------------------------------
			|The id of an unresolved ticket in the ticket_statuses table
			|
			|
			*/
			'unresolved_ticket_status_id'=>4,
			/*
			|--------------------------------------------------------------------------
			| Securable/System Object Permissions
			|--------------------------------------------------------------------------
			|Permissions to set on system objects.Will be used on the clientSide
			|
			|
			*/
			'secuable_permissions'=>array(

					array('canView'=>'Can View'),
					array('canInsert'=>'Can Insert'),
					array('canUpdate'=>'Can Update'),
					array('canDelete'=>'Can Delete'),
					array('canSearch'=>'Can Search'),
					array('canReport'=>'Can Report'),

				),
			/*
			|--------------------------------------------------------------------------
			| Modules Object Permissions
			|--------------------------------------------------------------------------
			|Permissions to set on system modules.Will be used on the clientSide
			|
			|
			*/
			'module_permissions'=>array(

					array('canView'=>'Can View'),
				),
			/*
			|--------------------------------------------------------------------------
			| Value to indicate whether the application should run in test mode or not
			|--------------------------------------------------------------------------
			|This config valud should be set to false if test are not being run
			|
			|
			*/
			'testing'=>false,
			/*
			|--------------------------------------------------------------------------
			| Closed state
			|--------------------------------------------------------------------------
			|This state will be used to represent the closed state on the profit percentages and 
			|purchase orders tables.
			|
			|
			*/
			'closed_state'=>0,
			/*
			|--------------------------------------------------------------------------
			| Opened state
			|--------------------------------------------------------------------------
			|This state will be used to represent the opened state on the profit percentages and 
			|purchase orders tables.
			*/
			'opened_state'=>1,
			/*
			|--------------------------------------------------------------------------
			| Default Page Number 
			|--------------------------------------------------------------------------
			|This state will be used to represent the opened state on the profit percentages and 
			|purchase orders table
			*/
			'default_page_size'=>25,
			/*
			|--------------------------------------------------------------------------
			| System Administrator Id
			|--------------------------------------------------------------------------
			|This id is the id of the administrator role.
			*/
			'admin_id'=>1,
			/*
			|--------------------------------------------------------------------------
			| True state
			|--------------------------------------------------------------------------
			|This state will be used to represent the true state on the  
			|purchase orders table.
			*/
			'true_state'=>1,
			/*
			|--------------------------------------------------------------------------
			| False state
			|--------------------------------------------------------------------------
			|This state will be used to represent the false state on the  
			|purchase orders table.
			*/
			'false_state'=>0,
			/*
			|--------------------------------------------------------------------------
			| Use File System
			|--------------------------------------------------------------------------
			|This config valud should be set to false if we decide to store images in the db
			|
			|
			*/
			'use_file_system'=>false,
			/*
			|--------------------------------------------------------------------------
			| Path for Uploaded Pictures
			|--------------------------------------------------------------------------
			|This config valud should be set to false if test are not being run
			|
			|
			*/
			//'pictures_path'=>'public/img/uploads/',
			'pictures_path' => path("public") . "uploads",
			/*
			|--------------------------------------------------------------------------
			| Path for Uploaded Pictures
			|--------------------------------------------------------------------------
			|This config valud should be set to false if test are not being run
			|
			|
			*/
			'picture_preview_path'=>'uploads',
			/*
			|--------------------------------------------------------------------------
			| court_discharge_type_id
			|--------------------------------------------------------------------------
			|This is the id of the court discharge type in our 
			|discharge types table.
			*/
			'court_discharge_type_id'=>1,
			/*
			|--------------------------------------------------------------------------
			| labour_discharge_type_id
			|--------------------------------------------------------------------------
			|This is the id of the labour discharge type in our 
			|discharge types table.
			*/
			'labour_discharge_type_id'=>2,
			/*
			|--------------------------------------------------------------------------
			| health_discharge_type_id
			|--------------------------------------------------------------------------
			|This is the id of the health discharge type in our 
			|discharge types table.
			*/
			'health_discharge_type_id'=>3,
			/*
			|--------------------------------------------------------------------------
			| release_discharge_type_id
			|--------------------------------------------------------------------------
			|This is the id of the release discharge type in our 
			|discharge types table.
			*/
			'release_discharge_type_id'=>4,
			/*
			|--------------------------------------------------------------------------
			| in_discharge_status_id
			|--------------------------------------------------------------------------
			|This is the id of the in discharge status in our 
			|discharge status table.
			*/
			'in_discharge_status_id'=>1,
			/*
			|--------------------------------------------------------------------------
			| out_discharge_status_id
			|--------------------------------------------------------------------------
			|This is the id of the out discharge status in our 
			|discharge status table.
			*/
			'out_discharge_status_id'=>2,
			/*
			|--------------------------------------------------------------------------
			| new_admission_type_id
			|--------------------------------------------------------------------------
			|This is the id of the new admission type id in our 
			|admission types table.
			*/
			'new_admission_type_id'=>1,
			/*
			|--------------------------------------------------------------------------
			| front face id
			|--------------------------------------------------------------------------
			|This is the id of the front view photo type
			*/
			'front_face'=>1,
			/*
			|--------------------------------------------------------------------------
			| left side id
			|--------------------------------------------------------------------------
			|This is the id of the left side view photo type
			*/
			'left_side'=>2,
			/*
			|--------------------------------------------------------------------------
			| right side id
			|--------------------------------------------------------------------------
			|This is the id of the right side view photo type
			*/
			'right_side'=>3,
			/*
			|--------------------------------------------------------------------------
			| discharged_inmate_status_id
			|--------------------------------------------------------------------------
			|This is the id of the discharged inmate status id in our 
			|admission types table.
			*/
			'discharged_inmate_status_id'=>1,
			/*
			|--------------------------------------------------------------------------
			| in_custody_inmate_status_id
			|--------------------------------------------------------------------------
			|This is the id of the in_custody inmate status id in our 
			|inmate status table.
			*/
			'in_custody_inmate_status_id'=>2,
			/*
			|--------------------------------------------------------------------------
			| released_inmate_status_id
			|--------------------------------------------------------------------------
			|This is the id of the released inmate status id in our 
			|inmate status table.
			*/
			'released_inmate_status_id'=>3,
			/*
			|--------------------------------------------------------------------------
			| escaped_inmate_status_id
			|--------------------------------------------------------------------------
			|This is the id of the escaped inmate status id in our 
			|inmate status table.
			*/
			'escaped_inmate_status_id'=>4,
			/*
			|--------------------------------------------------------------------------
			| dead_inmate_status_id
			|--------------------------------------------------------------------------
			|This is the id of the dead inmate status id in our 
			|inmate status table.
			*/
			'dead_inmate_status_id'=>5,
			/*
			|--------------------------------------------------------------------------
			| in_transit_inmate_status_id
			|--------------------------------------------------------------------------
			|This is the id of the in transit inmate status id in our 
			|inmate status table.
			*/
			'in_transit_inmate_status_id'=>6,
			/*
			|--------------------------------------------------------------------------
			| in_session_visit_status_id
			|--------------------------------------------------------------------------
			|This is the id of the in session visit status id in our 
			|visit status table.
			*/
			'in_session_visit_status_id'=>1,
			/*
			|--------------------------------------------------------------------------
			| escaped_escape_status_id
			|--------------------------------------------------------------------------
			|This is the id of the escaped status id in our 
			|escape status table.
			*/
			'escaped_escape_status_id'=>1,
			/*
			|--------------------------------------------------------------------------
			| in_session_visit_status_id
			|--------------------------------------------------------------------------
			|This is the id of the in captured status id in our 
			|escape status table.
			*/
			'captured_escape_status_id'=>2,
		
);
